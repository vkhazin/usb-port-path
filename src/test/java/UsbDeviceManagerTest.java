import com.sun.org.apache.xpath.internal.functions.WrongNumberArgsException;
import org.junit.jupiter.api.Test;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertThrows;


class UsbDeviceManagerTest {

    @Test
    void throwExceptionWhenWrongInputPass() {
        assertThrows(ParseException.class, () -> UsbDeviceManager.main("435fdf"),
                "Wrong input format should be  'device number'.");
    }

    @Test
    void throwExceptionWhenDeviceWasNotFound() {
        String deviceNumber = "65655";
        assertThrows(IllegalArgumentException.class, () -> UsbDeviceManager.main(deviceNumber),
                "Device with number '" + deviceNumber + "' wasn't found.");
    }

    @Test
    void throwExceptionWhenNoParametersPass() {
        assertThrows(WrongNumberArgsException.class, UsbDeviceManager::main,
                "Expected one argument 'device number'.");
    }

    @Test
    void throwExceptionWhenMoreThenOneParameterWerePass() {
        assertThrows(WrongNumberArgsException.class, () -> UsbDeviceManager.main("2", "3"),
                "Expected one argument 'device number'.");
    }
}
