import com.sun.org.apache.xpath.internal.functions.WrongNumberArgsException;
import org.usb4java.*;

import javax.swing.text.MaskFormatter;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;


public class UsbDeviceManager {

    private Context context;

    private UsbDeviceManager() {
        this.context = new Context();
    }

    private Context getContext() {
        return context;
    }

    static UsbDeviceManager getInstance() {
        UsbDeviceManager usbDeviceManager = new UsbDeviceManager();

        int result = LibUsb.init(usbDeviceManager.getContext());
        if (result != LibUsb.SUCCESS) {
            throw new LibUsbException("Unable to initialize LibUsb context.", result);
        }
        return usbDeviceManager;
    }


    public static void main(String... args) throws ParseException, WrongNumberArgsException {

        if (args.length != 1) {
            throw new WrongNumberArgsException("Expected one argument 'device number'.");
        }

        String stringNumber;
        try {

            MaskFormatter mask = new MaskFormatter("#####");
            stringNumber = mask.valueToString(args[0]);

        } catch (ParseException e) {
            throw new ParseException("Wrong input format should be  'device number'.", e.getErrorOffset());
        }

        int usbDeviceNumber = Integer.valueOf(stringNumber.replaceAll("\\D+", ""));
        UsbDeviceManager usbDeviceManager = UsbDeviceManager.getInstance();
        UsbIdentifier usbIdentifier = usbDeviceManager.getUsbIdentifier(usbDeviceNumber);
        System.out.println(usbIdentifier.getPath());
    }

    public UsbIdentifier getUsbIdentifier(Integer usbDevNumber) {

        Device targetDevice = getDeviceByNumber(usbDevNumber);

        UsbIdentifier usbIdentifier = new UsbIdentifier();
        usbIdentifier.setPath(getUsbIdentifierPath(targetDevice));
        usbIdentifier.setUsbDevice(getUsbDeviceData(targetDevice));
        return usbIdentifier;
    }

    private UsbDevice getUsbDeviceData(Device device) {
        UsbDevice usbDevice = new UsbDevice();

        DeviceDescriptor descriptor = getDeviceDescriptor(device);
        usbDevice.setBus(String.valueOf(LibUsb.getBusNumber(device)));
        usbDevice.setPort(String.valueOf(LibUsb.getPortNumber(device)));
        usbDevice.setDeviceClass(DescriptorUtils.getUSBClassName(descriptor.bDeviceClass()));
        usbDevice.setChildrenList(findUsbDeviceChildren(device));

        return usbDevice;
    }

    private List<UsbDevice> findUsbDeviceChildren(Device inputDevice) {
        DeviceList deviceList = getDevicesList();

        List<UsbDevice> deviceChildren = new ArrayList<>();

        for (Device device : deviceList) {

            if (device.equals(inputDevice)) {
                continue;
            }
            if (inputDevice.equals(LibUsb.getParent(device))) {
                UsbDevice child = getUsbDeviceData(device);
                deviceChildren.add(child);
            }
        }
        return deviceChildren;
    }

    private static DeviceDescriptor getDeviceDescriptor(Device device) {
        DeviceDescriptor descriptor = new DeviceDescriptor();
        int result = LibUsb.getDeviceDescriptor(device, descriptor);

        if (result < 0) {
            throw new LibUsbException(
                    "Unable to read device descriptor.", result);
        }
        return descriptor;
    }

    private static String getUsbIdentifierPath(Device device) {
        StringBuilder devicePath = new StringBuilder();
        devicePath.append("b").append(LibUsb.getBusNumber(device));

        final int BYTE_BUFFER_SIZE = 8;
        ByteBuffer path = BufferUtils.allocateByteBuffer(BYTE_BUFFER_SIZE);
        int result = LibUsb.getPortNumbers(device, path);

        if (result > 0) {
            for (int i = 0; i < result; i++) {
                devicePath.append(".p").append(path.get(i));
            }
        }
        return devicePath.toString();
    }

    private DeviceList getDevicesList() {
        DeviceList deviceList = new DeviceList();
        int result = LibUsb.getDeviceList(this.context, deviceList);

        if (result < 0) {
            throw new LibUsbException("Unable to get device list.", result);
        }
        return deviceList;
    }

    private Device getDeviceByNumber(Integer deviceNumber) {
        DeviceList deviceList = getDevicesList();
        for (Device device : deviceList) {
            if (LibUsb.getDeviceAddress(device) == deviceNumber) {
                return device;
            }
        }
        throw new IllegalArgumentException("Device with number '" + deviceNumber + "' wasn't found.");
    }
}
