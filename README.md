# usb-port-path

## Requirements

1. Create a Java class using openjdk version "1.8.0_181"
2. Provide a public static method of the class to return an abbriviated path to a usb devices
3. E.g. when `lsusb -t` returns following output
```
/:  Bus 02.Port 1: Dev 1, Class=root_hub, Driver=xhci_hcd/6p, 5000M
    |__ Port 2: Dev 2, If 0, Class=Hub, Driver=hub/4p, 5000M
/:  Bus 01.Port 1: Dev 1, Class=root_hub, Driver=xhci_hcd/7p, 480M
    |__ Port 2: Dev 2, If 0, Class=Hub, Driver=hub/4p, 480M
        |__ Port 4: Dev 6, If 1, Class=Human Interface Device, Driver=usbhid, 12M
        |__ Port 4: Dev 6, If 2, Class=Human Interface Device, Driver=usbhid, 12M
        |__ Port 4: Dev 6, If 0, Class=Human Interface Device, Driver=usbhid, 12M
        |__ Port 3: Dev 4, If 0, Class=Human Interface Device, Driver=usbhid, 1.5M
    |__ Port 3: Dev 3, If 0, Class=Hub, Driver=hub/4p, 480M
        |__ Port 2: Dev 5, If 0, Class=Wireless, Driver=btusb, 12M
        |__ Port 2: Dev 5, If 1, Class=Wireless, Driver=btusb, 12M
```
4. Input into the class method will be `Dev 6`
5. Output will be `b01.p2.p4`
6. JUnit tests are included
7. Readme.md is provided to outline pre-requisites setup and how to run the unit tests

## Design

```
public class UsbTest {
   public static void main(String[] args) {
      //source for pulling out id from args and calling getUniqueUSBIdentifier
     //System.out.println("for input of " + <input> + " got unique identifier " + <output
      }
   USBIdnentifier getUniqueUSBIdentifier(String usbDevNumber) {
    //fill me in
   }

USBIdentifier {
   String path;
   UsbDevice tree;
}

UsbDevice{
   String Bus;
   String Port;
   String Class;
   String Driver;
   UsbDevice[] children;
}

```
## How to contribute:

1. Fork this repo
2. Commit code to your own repo
3. Submit a pull request when ready

## How to test project using Maven:

   In Maven project the test phase in the build lifecycle is executed by the maven-surefire-plugin.  
   To start your test you need to invoke maven test phase: `mvn clean test` or `mvn clean install`
    
   You can read about it more:  
       [maven-surefire-plugin](https://maven.apache.org/surefire/maven-surefire-plugin/index.html)  
       [Maven lifecycle](https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html)
       
## How to run:  
  You can run this application as a console application.  
  Common form: `../path_to_application/>java -jar /target/UsbPortPath.jar 'device number'`  
  For example: `../path_to_application/>java -jar /target/UsbPortPath.jar 2`